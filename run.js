const bh = require('./businessHourDuration');

// Sunday 2am (02/17) - Sunday 6am (02/23) - 1 week apart - 40 hours
console.log("Example 1:");
var duration = bh.getDuration("1550386800","1550919600");
console.log(duration + "\n");

//Same Day 7:22am - 12:00pm - 3 hours 
console.log("Example 2:");
var duration = bh.getDuration("1550578920","1550595600");
console.log(duration + "\n");

// Monday 4am - Weds 6pm - Three Full Days - 24 hours
console.log("Example 3:");
var duration = bh.getDuration("1550480400","1550703600");
console.log(duration + "\n");

// Monday 12:30pm - Tuesday 1:30pm - One Full Day Plus One Hour - 9 hours
console.log("Example 4:");
var duration = bh.getDuration("1550511000","1550601000");
console.log(duration + "\n");	

//Monday 1pm - Weds 6pm - Two Full Days, One Half Day - 20 hours
console.log("Example 5:");
var duration = bh.getDuration("1550512800","1550703600");
console.log(duration + "\n");

//Weds 6pm - Monday 1pm - End is before start - Throws Error
console.log("Example 6:");
var duration = bh.getDuration("1550703600","1550512800");
console.log(duration + "\n");

//Same Day 7:22am - 8:22am - 0 hours (both outside range)
console.log("Example 7:");
var duration = bh.getDuration("1550578920","1550582520");
console.log(duration+ "\n");
 	
//Same Day 7:22am - 12:11:22pm - 3 hours, 11 minutes, 22 seconds 
console.log("Example 8:");
var duration = bh.getDuration("1550578920","1550596282");
console.log(duration + "\n");

