//Author: Rachel Fee
//Business Hour Duration
//Node v10.15.1

//This node function is used to find the duration between two timepoints, inclusive of business hours. Second accuracy.
//IE if business hours are configured as 9am to 5pm in the configuration section, 
//then noon Monday to noon Tuesday would be a duration of 8 hours of business time.

//Can be run from run.js, which includes examples.

//accepts a start timestamp and end timestamp in unix format
//I chose unix as it is the easiest to store and most universal format (no confusion with time/date formats)
//returns the timespan in hours

//NOTE --
//Be aware of TIMEZONE when passing in unix timestamp. Unix Timestamps are UTC

//Limitations --
//Business Hour start and end must be within the same day

//imports
const moment = require('moment-timezone');

//configuration
moment.tz("America/Toronto"); //set the timezone from https://en.wikipedia.org/wiki/List_of_tz_database_time_zones, using "TZ Database Name"

var businessStartConfig = "09:00:00"; //24 hour time in format "HH:mm:ss., eg 09:00:00" 
var businessEndConfig = "17:00:00"; //24 hour time in format "HH:mm:ss, eg 17:00:00"
var weekendDays = [0, 6]; //Array of weekend days. eg. 0 = Sunday, 6 = Saturday. Can specify any number of days.

//alternate configuration for testing
// var businessStartConfig = "09:00:00"; 
// var businessEndConfig = "17:00:01"; //adds one second to the end of each day
// var weekendDays = [0, 2, 6]; //adds an additional weekend day, on Tuesday

//utility function for comparisons
var secondsOfDay = function(m){

    return m.seconds() + (m.minutes() * 60 ) +  (m.hours() * 60 * 60) ; 
}

//accepts seconds and returns a readable string
var makeReadable = function(s){

    var hours = Math.floor(s / 3600);
    var minutes = Math.floor((s - (3600 * hours))/60);
    var seconds = s - (3600 * hours) - (60 * minutes);

    return hours + " hours, " + minutes + " minutes, " + seconds + " seconds."
}


module.exports = {
    
    getDuration: function (startTS, endTS) {

        //convert start and end unix timestamp to moments
        var start = moment.unix(startTS); 
        var end = moment.unix(endTS);

        //ensure that dates passed are valid
        if (!start.isValid() || !end.isValid()) return "ERROR: Invalid start or end date passed.";
        //ensure start time is earlier than end time
        if (start > end) return "ERROR: End is before start.";

        //check config values are valid and get duration of a work day.
        var businessStart = moment(businessStartConfig, "HH:mm:ss");
        var businessEnd = moment(businessEndConfig, "HH:mm:ss");

        //ensure that start and end time are valid
        if (!businessStart.isValid() || !businessEnd.isValid()) return "ERROR: Invalid business hours passed.";

        //duration of a single work day in seconds
        var workDayDuration = moment.duration(businessEnd.diff(businessStart)).asSeconds();

        //ensure start time is earlier than end time
        if (workDayDuration < 0) return "ERROR: Work day end is earlier than start.";
        

        //variable to hold the results in seconds
        var totalSeconds = 0;

        //calculate the seconds if range is only a single day
        if (start.isSame(end,'day')){
            
            //not a weekend
            if (!weekendDays.includes(start.day())){
                //business end - business start. If one is outside range, use the business start/end
                var totalSeconds = (secondsOfDay(end) > secondsOfDay(businessEnd) ? secondsOfDay(businessEnd) : secondsOfDay(end)) - (secondsOfDay(start) < secondsOfDay(businessStart) ? secondsOfDay(businessStart) : secondsOfDay(start));
                
                //will be a negative if both are outside range. Reset to 0.
                if (totalSeconds < 0) totalSeconds = 0;
            }
        } else { //calculate the seconds if range is more than one day

            //add minutes of first day 
            if ( (secondsOfDay(start) < secondsOfDay(businessStart)) && !weekendDays.includes(start.day())){ //start is before business start && not a weekend
                totalSeconds += workDayDuration;
            }else if ((secondsOfDay(businessStart) < secondsOfDay(start) && secondsOfDay(start) < secondsOfDay(businessEnd)) && !weekendDays.includes(start.day())){ //witihin business day && not a weekend
                //add diff of businessEnd to start
                totalSeconds += (secondsOfDay(businessEnd) - secondsOfDay(start));
            }  
            //else do nothing, start is later than valid range, or is a weekend. 0 minutes for today

            //add minutes of last day
            if ( secondsOfDay(end) > secondsOfDay(businessEnd) && !weekendDays.includes(start.day())){ //end after end time && not a weekend
                totalSeconds += workDayDuration;
            }else if ((secondsOfDay(businessStart) < secondsOfDay(end) && secondsOfDay(end) < secondsOfDay(businessEnd)) && !weekendDays.includes(start.day())){ //within business day && not a weekend
                //add diff of businessstart to end
                totalSeconds += (secondsOfDay(end) - secondsOfDay(businessStart));
            }  
            //else do nothing, end is eariier than valid range. 0 minutes for today

            //loop through day 2 to end-1, each of these will be a full day. Block will be skipped if range is only 2 days
            for (var d = moment(start).add(1, 'days'); end.diff(d, 'days') >= 1; d.add(1, 'days')) {
                if (!weekendDays.includes(d.day())){
                    totalSeconds += workDayDuration; //add the seconds in one workday
                }                    
            }
        }
        return makeReadable(totalSeconds);
    }

};
