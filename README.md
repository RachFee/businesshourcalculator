## Business Hour Calculator
Author: Rachel Fee

A calculator package to calculate the business hour duration between two timestamps.

**Run Instructions**

1. After downloading, install dependencies with 
	npm install 
2. Sample data is included in run.js. Navigate to the directory in the command line and run with
	node run.js

**Configuration Instructions** 

1. Open businessHourDuration.js. 
2. At the top of the file, you'll find a configuration section that may be edited with your choice of data. 
3. A second sample configuration section is included with alternate config options.